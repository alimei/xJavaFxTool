DirectoryTreeTool 文件列表生成器

#### 项目简介：
DirectoryTreeTool是使用javafx开发的一款文件列表生成器，能自定义生成目录列表结构，可用于项目代码结构展示及目录结构说明。

#### 功能特色
- 可自定义目录深度，过滤文件和目录类型
- 可选择是否显示隐藏文件及不显示空目录
- 可自定义添加各种目录及文件过滤规则
- 可选择显示目录结构内容，包括文件大小、最后修改时间、权限等

**xJavaFxTool交流QQ群：== [387473650(此群已满)](https://jq.qq.com/?_wv=1027&k=59UDEAD) 请加群②[1104780992](https://jq.qq.com/?_wv=1027&k=bhAdkju9) ==**

#### 环境搭建说明：
- 开发环境为jdk1.8，基于maven构建
- 使用eclipase或Intellij Idea开发(推荐使用[Intellij Idea](https://www.jetbrains.com/?from=xJavaFxTool))
- 该项目为javaFx开发的实用小工具集[xJavaFxTool](https://gitee.com/xwintop/xJavaFxTool)的插件。
- 本项目使用了[lombok](https://projectlombok.org/),在查看本项目时如果您没有下载lombok 插件，请先安装,不然找不到get/set等方法
- 依赖的[xcore包](https://gitee.com/xwintop/xcore)已上传至git托管的maven平台，git托管maven可参考教程(若无法下载请拉取项目自行编译)。[教程地址：点击进入](http://blog.csdn.net/u011747754/article/details/78574026)

![文件列表生成器.png](images/文件列表生成器.png)
![文件列表生成器.gif](images/文件列表生成器.gif)
